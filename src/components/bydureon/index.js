import React, { Component } from "react";
import "../../global.css";
import Header from "../header";
import Footer from "../footer";

class Bydureon extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    let username = sessionStorage.getItem("username");
    if(username == null){
      window.location = "/videoapp/";
    }
  }

  showChatRoom = e => {
    this.props.history.push("/videoapp/videochat");
  };

  render() {
    return (
      <div>
        <Header title="Welcome to CVRM Congress - Bydureon"/>
        <div style={{ textAlign: "center" }}>
          <div
            style={{
              marginTop: 40,
              color: "#000080",
              fontSize: "16px",
              fontWeight: "bold"
            }}
          >

          </div>
          <div style={{ marginTop: 40, marginBottom: 40 }}>
            <button className="btn-style" onClick={this.showChatRoom}>
              MSL Chat
            </button>
            <button className="btn-style">
              Event Library
            </button>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default Bydureon;
