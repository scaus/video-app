import React, { Component } from "react";
import "./styles.css";
import Header from "../header";
import Footer from "../footer";

class VirtualCongress extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    let username = sessionStorage.getItem("username");
    if(username == null){
      window.location = "/videoapp/";
    }
  }

  showChatRoom = e => {
    this.props.history.push("/videoapp/videochat");
  };

  showBooths = e => {
    this.props.history.push("/videoapp/booths");
  };

  render() {
    return (
      <div>
        <Header title="Welcome To Virtual CVRM Congress"/>
        <div style={{ textAlign: "center" }}>
          <div
            style={{
              marginTop: 40,
              color: "#000080",
              fontSize: "16px",
              fontWeight: "bold"
            }}
          >

          </div>
          <div style={{ marginTop: 40, marginBottom: 40 }}>
            <button className="btn-style" onClick={this.showBooths}>Booths</button>
            <button className="btn-style">Lounge</button>
            <button className="btn-style">Keynote</button>
            <button className="btn-style" onClick={this.showChatRoom}>
              Chat
            </button>
          </div>
          <div>
            <iframe
              src="https://www.youtube.com/embed/0224GYA19uw"
              frameBorder="0"
              allow="autoplay; encrypted-media"
              allowFullScreen={true}
              title="video"
              width="680px"
              height="460px"
            />
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default VirtualCongress;
