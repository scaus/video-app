import React, { Component } from "react";
import "../../global.css";
import "./styles.css";
import Header from "../header";
import Footer from "../footer";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: ""
    }
  }

  componentDidMount() {
    let username = sessionStorage.getItem("username");
    if(username != null){
      this.props.history.push("/videoapp/virtualcongress");
    }
  }

  handleUsername = (e) => {
    this.setState({
      username: e.target.value
    });
  };

  handlePassword = (e) => {
    this.setState({
      password: e.target.value
    });
  };

  loginBtnClick = (e) => {
    if(this.state.username == "" || this.state.password == ""){
      alert("Please enter a username and password");
      return;
    }
    sessionStorage.setItem("username", this.state.username)
    this.props.history.push("/videoapp/virtualcongress");
  };

  render() {
    return (
      <div>
        <Header title="Virtual CVRM Congress - Login"/>
          <div className="login-view">
            <input type="text" onChange={this.handleUsername} className="txt-style" placeholder="username"/>
            <input type="password" onChange={this.handlePassword} className="txt-style" placeholder="password"/>
            <button className="btn-style" style={{marginTop: "10px", width: "200px", padding: "5px", borderRadius: "10px"}} onClick={this.loginBtnClick}>Login</button>
          </div>
        <Footer />
      </div>
    );
  }
}

export default Login;
