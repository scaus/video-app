import React, { Component } from "react";
import "./styles.css";

const logo = require("../../assets/images/az-logo-white.svg");

class Footer extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {}

  render() {
    return (
      <div className="Footer">
        <div className="Footer-logo-view">
          <img className="Footer-logo" src={logo} />
        </div>
        <div className="Copyright">
          © Astrazeneca 2020
        </div>
      </div>
    );
  }
}

export default Footer;
