import React, { Component } from "react";
import "./styles.css";
import { Link } from "react-router-dom";

const logo = require("../../assets/images/az-logo-white.svg");
const userIcon = require("../../assets/images/user-icon.png");

class Header extends Component {
  constructor(props) {
    super(props);
  }

  logout = () => {
    var n = sessionStorage.length;
    while (n--) {
      var key = sessionStorage.key(n);
      sessionStorage.removeItem(key);
    }
    window.location = "/videoapp/";
  };

  render() {
    return (
      <div className="header">
        <div className="logo-view">
          <Link to="/videoapp/">
            <img className="logo" src={logo} />
          </Link>
        </div>
        <label className="label-text">{this.props.title}</label>
        <div className="header-right-view" onClick={this.logout}>
          <img
            className="user-icon"
            src={sessionStorage.getItem("username") != null
              ? userIcon
              : ""}
          />
          <label className="user-text">
            {sessionStorage.getItem("username") != null
              ? sessionStorage.getItem("username")
              : ""}
          </label>
        </div>
      </div>
    );
  }
}

export default Header;
