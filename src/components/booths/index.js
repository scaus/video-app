import React, { Component } from "react";
import "../../global.css";
import Header from "../header";
import Footer from "../footer";

class Booths extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    let username = sessionStorage.getItem("username");
    if(username == null){
      window.location = "/videoapp/";
    }
  }

  showBydureon = e => {
    this.props.history.push("/videoapp/bydureon");
  };

  render() {
    return (
      <div>
        <Header title="Welcome to CVRM Congress - Booths"/>
        <div style={{ textAlign: "center" }}>
          <div
            style={{
              marginTop: 40,
              color: "#000080",
              fontSize: "16px",
              fontWeight: "bold"
            }}
          >

          </div>
          <div style={{ marginTop: 40, marginBottom: 40 }}>
            <button className="btn-style">
              Farxiga
            </button>
            <button className="btn-style" onClick={this.showBydureon}>
              Bydureon
            </button>
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}

export default Booths;
