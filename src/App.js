import React from "react";
import { Route, Switch, Router } from "react-router-dom";
import { Booths, Bydureon, VideoChat, VirtualCongress, Login } from "./components";
import { createBrowserHistory } from "history";
const history = createBrowserHistory();

const Routes = () => {
  return (
    <Router basename={"/claimscheck"} history={history}>
      <div className="App">
        <Route exact path={"/videoapp"} component={Login} />
        <Route path={"/videoapp/Booths"} component={Booths} />
        <Route path={"/videoapp/videochat"} component={VideoChat} />
        <Route path={"/videoapp/virtualcongress"} component={VirtualCongress} />
        <Route path={"/videoapp/bydureon"} component={Bydureon} />
      </div>
    </Router>
  );
};

export default Routes;
